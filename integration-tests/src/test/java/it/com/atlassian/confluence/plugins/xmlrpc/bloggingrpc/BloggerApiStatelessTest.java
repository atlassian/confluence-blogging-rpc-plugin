package it.com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.api.model.Expansion;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.Version;
import com.atlassian.confluence.api.model.content.id.ContentId;
import com.atlassian.confluence.api.model.link.LinkType;
import com.atlassian.confluence.api.service.content.SpaceService;
import com.atlassian.confluence.test.BaseUrlSelector;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessRestTestRunner;
import com.atlassian.confluence.test.stateless.ResetFixtures;
import com.atlassian.confluence.test.stateless.fixtures.BlogPostFixture;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import org.apache.commons.lang3.StringUtils;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.BlogPostFixture.blogFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.confluence.webdriver.pageobjects.utils.SanitationUtils.urlEncode;
import static it.com.atlassian.confluence.plugins.xmlrpc.bloggingrpc.TestUtils.toXmlRpcParams;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;

@RunWith(ConfluenceStatelessRestTestRunner.class)
public class BloggerApiStatelessTest {

    private static final String BLOG_CONTENTS_1 = "Test Blog Content";
    private static final String BLOG_CONTENTS_2 = "Another Blog Content";
    private static final String BLOG_CONTENTS_EDITED = "Edited Blog Content";

    // Title contains punctuation to ensure blog URL consistency across
    // Confluence versions and dates.
    // Punctuation in the title will prevent "pretty" blog URLs: .../YYYY/MM/DD/Title
    private static final String BLOG_TITLE_1 = "Test Blog!";
    private static final String BLOG_TITLE_2 = "Another Test Blog!";

    private static final String MESSAGE_CANNOT_SAVE_DRAFT =
            "You cannot save a draft version through the Confluence " +
            "Blogging API. You must publish your updates immediately.";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Inject
    private static BaseUrlSelector baseUrlSelector;
    @Inject
    private static ConfluenceRestClient restClient;

    @Fixture
    private static UserFixture user = userFixture().build();

    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();

    @Fixture
    private static BlogPostFixture blog = blogFixture()
            .author(user)
            .space(space)
            .title(BLOG_TITLE_1)
            .content(BLOG_CONTENTS_1)
            .version(Version.builder().when(new Date()).build())
            .build();
    @Fixture
    private static BlogPostFixture anotherBlog = blogFixture()
            .author(user)
            .space(space)
            .title(BLOG_TITLE_2)
            .content(BLOG_CONTENTS_2)
            .version(Version.builder().when(new Date()).build())
            .build();

    private static XmlRpcClient xmlRpcClient;

    @BeforeClass
    public static void initialise() throws MalformedURLException {
        xmlRpcClient = new XmlRpcClient(baseUrlSelector.getBaseUrl() + "/rpc/xmlrpc");
        final SpaceService spaceService = restClient.getAdminSession().spaceService();
        spaceService.find().withKeys("ds").fetch().ifPresent(spaceService::delete);
    }

    @Test
    public void testCreateDraftBlog() throws IOException, XmlRpcException {
        thrown.expect(XmlRpcException.class);
        thrown.expectMessage(MESSAGE_CANNOT_SAVE_DRAFT);
        createNewBlog(space.get().getKey(),
                BLOG_TITLE_1,
                BLOG_CONTENTS_1,
                false);
    }

    @Test
    public void testUpdateDraftBlog() throws IOException, XmlRpcException {
        Content content = restClient.createSession(user.get())
                .contentService()
                .find()
                .withId(blog.get().getId())
                .fetchOrNull();

        assertNotNull(content);

        thrown.expect(XmlRpcException.class);
        thrown.expectMessage(MESSAGE_CANNOT_SAVE_DRAFT);
        updateBlog(content.getId().asLong(),
                BLOG_TITLE_1,
                BLOG_CONTENTS_EDITED,
                false);
    }

    @Test
    public void testCreateBlog() throws XmlRpcException, IOException {
        final String blogId = createNewBlog(
                space.get().getKey(),
                "New " + BLOG_TITLE_1, BLOG_CONTENTS_1,
                true).toString();

        Content content = restClient.createSession(user.get())
                .contentService()
                .find(new Expansion("body.view"), new Expansion("version"))
                .withId(ContentId.of(Long.parseLong(blogId)))
                .fetchOrNull();
        assertNotNull(content);
        assertThat(
                content.getBody().get(ContentRepresentation.VIEW).getValue(),
                containsString(BLOG_CONTENTS_1)
        );
        assertBlogUrl(content);
    }

    @Test
    public void testCreateBlogWithoutTitle() throws XmlRpcException, IOException {
        thrown.expect(XmlRpcException.class);
        thrown.expectMessage("You must supply a title to post a blog page");
        createNewBlog(space.get().getKey(), null, BLOG_CONTENTS_1, true);
    }

    @Test
    public void testCreateBlogWithDuplicateTitleInTheSamePublishedDay() throws XmlRpcException, IOException {
        thrown.expect(XmlRpcException.class);
        thrown.expectMessage("A blog post with title {0} already exists for date:");
        createNewBlog(space.get().getKey(),
                BLOG_TITLE_1,
                BLOG_CONTENTS_1,
                true);
    }

    @ResetFixtures(value = {"blog"}, when = ResetFixtures.When.AFTER)
    @Test
    public void testEditBlog() throws XmlRpcException, IOException {
        final Object updateSuccessful = updateBlog(
                blog.get().getId().asLong(),
                BLOG_TITLE_1,
                BLOG_CONTENTS_EDITED,
                true);

        assertThat(updateSuccessful, is(Boolean.TRUE));

        Content content = restClient.createSession(user.get())
                .contentService()
                .find(new Expansion("body.view"))
                .withId(blog.get().getId())
                .fetchOrNull();
        assertNotNull(content);
        assertThat(
                content.getBody().get(ContentRepresentation.VIEW).getValue(),
                containsString(BLOG_CONTENTS_EDITED)
        );
        assertBlogUrl(blog.get());
    }

    @Test
    public void testEditBlogWithoutTitle() throws XmlRpcException, IOException {
        thrown.expect(XmlRpcException.class);
        thrown.expectMessage("You must supply a title to post a blog page");
        updateBlog(
                blog.get().getId().asLong(),
                null,
                BLOG_CONTENTS_EDITED,
                true);
    }

    @Test
    public void testEditBlogWithDuplicateTitleInTheSamePublishedDay() throws XmlRpcException, IOException {
        thrown.expect(XmlRpcException.class);
        thrown.expectMessage("A blog post with title {0} already exists for date:");
        updateBlog(
                anotherBlog.get().getId().asLong(),
                BLOG_TITLE_1,
                BLOG_CONTENTS_EDITED,
                true);
    }

    @Test
    public void testGetBlogPost() throws XmlRpcException, IOException {
        final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
        final Map<?, ?> blogStructure = (Map) xmlRpcClient.execute("blogger.getPost", new Vector<>(
                asList(
                        new Object[]{
                                StringUtils.EMPTY,
                                blog.get().getId().serialise(),
                                user.get().getUsername(),
                                user.get().getPassword()
                        }
                )
        ));

        assertNotNull(blogStructure);

        assertThat(
                blogStructure,
                allOf(
                        hasEntry("postid", blog.get().getId().serialise()),
                        hasEntry("blogid", space.get().getKey()),
                        hasEntry("title", blog.get().getTitle()),
                        hasEntry(
                                "content",
                                format(
                                        "<title>%s</title>%s",
                                        blog.get().getTitle(),
                                        BLOG_CONTENTS_1
                                )
                        ),
                        hasEntry("authorName", user.get().getUsername()),
                        hasEntry("authorEmail", user.get().getEmail())
                )
        );

        Date dateCreated = (Date) blogStructure.get("dateCreated");
        LocalDate localDate = dateCreated.toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDate();
        if (restClient.getAdminSession().darkFeature().isSiteFeatureEnabled("confluence.readable.url")) {
            assertThat(
                    blogStructure, hasEntry("url",
                            format(
                                    "%s/spaces/%s/blog/%s/%d/%s",
                                    baseUrlSelector.getBaseUrl(),
                                    space.get().getKey(),
                                    dateTimeFormatter.format(localDate),
                                    blog.get().getId().asLong(),
                                    urlEncode(blog.get().getTitle().replaceAll("!", ""))
                            )));
        } else {
            assertThat(
                    blogStructure, hasEntry("url",
                            format(
                                    "%s/pages/viewpage.action?pageId=%d",
                                    baseUrlSelector.getBaseUrl(),
                                    blog.get().getId().asLong()
                            )));
        }

        assertThat(
                dateTimeFormatter.format(localDate),
                is(blog.get().getVersion().getWhenAt().format(dateTimeFormatter))
        );
    }

    @ResetFixtures(value = {"space", "blog"}, when = ResetFixtures.When.BOTH)
    @Test
    public void testGetUsersBlog() throws XmlRpcException, IOException {
        final List<?> usersBlogs = (List) xmlRpcClient.execute(
                "blogger.getUsersBlogs",
                new Vector<>(
                        asList(
                                new Object[]{
                                        StringUtils.EMPTY,
                                        user.get().getUsername(),
                                        user.get().getPassword(),
                                }
                        )
                )
        );

        assertNotNull(usersBlogs);
        System.out.println(usersBlogs);
        assertThat(usersBlogs, hasSize(1));

        final Map<?, ?> blogStruct = (Map) usersBlogs.get(0);

        assertThat(
                blogStruct,
                allOf(
                        hasEntry("blogName", space.get().getName()),
                        hasEntry(
                                "url",
                                format(
                                        "%s/pages/viewrecentblogposts.action?key=%s",
                                        baseUrlSelector.getBaseUrl(),
                                        space.get().getKey()
                                )
                        ),
                        hasEntry("blogid", space.get().getKey())
                )
        );
    }

    @Test
    public void testGetUserInfo() throws XmlRpcException, IOException {
        final Map<?, ?> userInfo = (Map) xmlRpcClient.execute(
                "blogger.getUserInfo",
                new Vector<>(
                        asList(
                                new Object[]{
                                        StringUtils.EMPTY,
                                        user.get().getUsername(),
                                        user.get().getPassword()
                                }
                        )
                )
        );

        assertNotNull(userInfo);

        assertThat(
                userInfo,
                allOf(
                        hasEntry("firstname", user.get().getFullName()),
                        hasEntry("email", user.get().getEmail()),
                        hasEntry("userid", user.get().getUsername())
                )
        );
    }

    private void assertBlogUrl(Content content) {
        if (restClient.getAdminSession().darkFeature().isSiteFeatureEnabled("confluence.readable.url")) {
            final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            String creationDate = content.getVersion().getWhenAt().format(dateTimeFormatter);
            assertThat(
                    content.getLinks().get(LinkType.WEB_UI).getPath(),
                            containsString(format("/spaces/%s/blog/%s/%d/%s", space.get().getKey(), creationDate, content.getId().asLong(), urlEncode(content.getTitle().replaceAll("!", ""))))
            );
        } else {
            assertThat(
                    content.getLinks().get(LinkType.WEB_UI).getPath(),
                    is(format("/pages/viewpage.action?pageId=%s", content.getId().serialise()))
            );
        }
    }

    private Object createNewBlog(final String spaceKey,
                                 final String title,
                                 final String content,
                                 final boolean publish)
            throws XmlRpcException, IOException {
        final String blogBody =
                (title != null ? "<title>" + title + "</title>" : "") + content;
        return xmlRpcClient.execute("blogger.newPost", toXmlRpcParams(
                new Object[]{
                        StringUtils.EMPTY,
                        spaceKey,
                        user.get().getUsername(),
                        user.get().getPassword(),
                        blogBody,
                        publish ? Boolean.TRUE : Boolean.FALSE
                })
        );
    }

    private Object updateBlog(final long blogPostId, final String title, final String content, final boolean publish)
            throws XmlRpcException, IOException {
        final String blogBody =
                (title != null ? "<title>" + title + "</title>" : "") + content;
        return xmlRpcClient.execute("blogger.editPost", toXmlRpcParams(
                new Object[]{
                        org.apache.commons.lang.StringUtils.EMPTY,
                        String.valueOf(blogPostId),
                        user.get().getUsername(),
                        user.get().getPassword(),
                        blogBody,
                        publish ? Boolean.TRUE : Boolean.FALSE
                })
        );
    }
}
