package it.com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.api.model.Expansion;
import com.atlassian.confluence.api.model.content.Content;
import com.atlassian.confluence.api.model.content.ContentRepresentation;
import com.atlassian.confluence.api.model.content.Label;
import com.atlassian.confluence.api.model.content.Version;
import com.atlassian.confluence.api.model.content.id.ContentId;
import com.atlassian.confluence.api.model.link.LinkType;
import com.atlassian.confluence.test.BaseUrlSelector;
import com.atlassian.confluence.test.rest.api.ConfluenceRestClient;
import com.atlassian.confluence.test.stateless.ConfluenceStatelessRestTestRunner;
import com.atlassian.confluence.test.stateless.ResetFixtures;
import com.atlassian.confluence.test.stateless.fixtures.BlogPostFixture;
import com.atlassian.confluence.test.stateless.fixtures.Fixture;
import com.atlassian.confluence.test.stateless.fixtures.PageFixture;
import com.atlassian.confluence.test.stateless.fixtures.SpaceFixture;
import com.atlassian.confluence.test.stateless.fixtures.UserFixture;
import org.apache.commons.lang3.StringUtils;
import org.apache.xmlrpc.XmlRpcClient;
import org.apache.xmlrpc.XmlRpcException;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import static com.atlassian.confluence.test.rpc.api.permissions.SpacePermission.REGULAR_PERMISSIONS;
import static com.atlassian.confluence.test.stateless.fixtures.BlogPostFixture.blogFixture;
import static com.atlassian.confluence.test.stateless.fixtures.PageFixture.pageFixture;
import static com.atlassian.confluence.test.stateless.fixtures.SpaceFixture.spaceFixture;
import static com.atlassian.confluence.test.stateless.fixtures.UserFixture.userFixture;
import static com.atlassian.confluence.webdriver.pageobjects.utils.SanitationUtils.urlEncode;
import static it.com.atlassian.confluence.plugins.xmlrpc.bloggingrpc.TestUtils.toXmlRpcParams;
import static java.lang.String.format;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;

@RunWith(ConfluenceStatelessRestTestRunner.class)
public class MetaWeblogApiStatelessTest {

    private static final String BLOG_CONTENTS_1 = "Test Blog Content";
    private static final String BLOG_CONTENTS_2 = "Another Blog Content";
    private static final String BLOG_CONTENTS_EDITED = "Edited Blog Content";

    // Title contains punctuation to ensure blog URL consistency across
    // Confluence versions and dates.
    // Punctuation in the title will prevent "pretty" blog URLs: .../YYYY/MM/DD/Title
    private static final String BLOG_TITLE_1 = "Test Blog!";
    private static final String BLOG_TITLE_2 = "Another Test Blog!";

    private static final String MESSAGE_CANNOT_SAVE_DRAFT =
            "You cannot save a draft version through the Confluence " +
            "Blogging API. You must publish your updates immediately.";

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Inject
    private static BaseUrlSelector baseUrlSelector;
    @Inject
    private static ConfluenceRestClient restClient;

    @Fixture
    private static UserFixture user = userFixture().build();

    @Fixture
    private static SpaceFixture space = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();
    @Fixture
    private static SpaceFixture anotherSpace = spaceFixture()
            .permission(user, REGULAR_PERMISSIONS)
            .build();

    @Fixture
    private static BlogPostFixture blog = blogFixture()
            .author(user)
            .space(space)
            .title(BLOG_TITLE_1)
            .content(BLOG_CONTENTS_1)
            .version(Version.builder().when(new Date()).build())
            .build();

    @Fixture
    private static BlogPostFixture anotherBlog = blogFixture()
            .author(user)
            .space(space)
            .title(BLOG_TITLE_2)
            .content(BLOG_CONTENTS_2)
            .version(Version.builder().when(new Date()).build())
            .build();

    @Fixture
    private static PageFixture page = pageFixture()
            .author(user)
            .space(space)
            .title("Confluence Overview")
            .build();
    @Fixture
    private static PageFixture anotherPage = pageFixture()
            .author(user)
            .space(anotherSpace)
            .title("Another Page")
            .build();


    private static XmlRpcClient xmlRpcClient;

    @BeforeClass
    public static void initialise() throws MalformedURLException {
        xmlRpcClient = new XmlRpcClient(baseUrlSelector.getBaseUrl() + "/rpc/xmlrpc");
    }

    @Test
    public void testCreateDraftBlog() throws IOException, XmlRpcException {
        thrown.expect(XmlRpcException.class);
        thrown.expectMessage(MESSAGE_CANNOT_SAVE_DRAFT);
        createNewBlog(space.get().getKey(),
                BLOG_TITLE_1,
                BLOG_CONTENTS_1,
                null,
                false);
    }

    @Test
    public void testUpdateDraftBlog() throws IOException, XmlRpcException {
        thrown.expect(XmlRpcException.class);
        thrown.expectMessage(MESSAGE_CANNOT_SAVE_DRAFT);
        updateBlog(blog.get().getId().asLong(),
                BLOG_TITLE_1,
                BLOG_CONTENTS_EDITED,
                null,
                false);
    }

    @Test
    public void testCreateBlog() throws XmlRpcException, IOException {
        final String blogId = createNewBlog(
                space.get().getKey(),
                "New" + BLOG_TITLE_1, BLOG_CONTENTS_1,
                null,
                true).toString();

        Content content = restClient.createSession(user.get())
                .contentService()
                .find(new Expansion("body.view"), new Expansion("version"))
                .withId(ContentId.of(Long.parseLong(blogId)))
                .fetchOrNull();
        assertNotNull(content);
        assertThat(
                content.getBody().get(ContentRepresentation.VIEW).getValue(),
                containsString(BLOG_CONTENTS_1)
        );
        assertBlogUrl(content);
    }

    @Test
    public void testCreateBlogWithoutTitle() throws XmlRpcException, IOException {
        thrown.expect(XmlRpcException.class);
        thrown.expectMessage("You must supply a title to post a blog page");
        createNewBlog(space.get().getKey(),
                StringUtils.EMPTY,
                BLOG_CONTENTS_1,
                null,
                true);
    }

    @Test
    public void testCreateBlogWithDuplicateTitleInTheSamePublishedDay() throws XmlRpcException, IOException {
        thrown.expect(XmlRpcException.class);
        thrown.expectMessage("A blog post with title {0} already exists for date:");
        createNewBlog(space.get().getKey(),
                BLOG_TITLE_1,
                BLOG_CONTENTS_1,
                null,
                true);
    }

    @ResetFixtures(value = {"blog"}, when = ResetFixtures.When.AFTER)
    @Test
    public void testEditBlog() throws XmlRpcException, IOException {
        final Object updateSuccessful = updateBlog(
                blog.get().getId().asLong(),
                BLOG_TITLE_1, BLOG_CONTENTS_EDITED,
                null,
                true);
        assertThat(updateSuccessful, is(Boolean.TRUE));

        Content content = restClient.createSession(user.get())
                .contentService()
                .find(new Expansion("body.view"))
                .withId(blog.get().getId())
                .fetchOrNull();
        assertNotNull(content);
        assertThat(
                content.getBody().get(ContentRepresentation.VIEW).getValue(),
                containsString(BLOG_CONTENTS_EDITED)
        );

        assertBlogUrl(blog.get());
    }

    @ResetFixtures(value = {"blog"}, when = ResetFixtures.When.AFTER)
    @Test
    public void testEditBlogWithoutTitle() throws XmlRpcException, IOException {
        updateBlog(
                blog.get().getId().asLong(),
                StringUtils.EMPTY,
                BLOG_CONTENTS_EDITED,
                null,
                true);
        /* Unlike the Blogger API, blog edits with blank title will not change the original blog title */
        Content content = restClient.createSession(user.get())
                .contentService()
                .find(new Expansion("body.view"))
                .withId(blog.get().getId())
                .fetchOrNull();
        assertNotNull(content);
        assertThat(
                content.getBody().get(ContentRepresentation.VIEW).getValue(),
                containsString(BLOG_CONTENTS_EDITED)
        );

        assertBlogUrl(blog.get());
    }

    @Test
    public void testEditBlogWithDuplicateTitleInTheSamePublishedDay() throws XmlRpcException, IOException {
        thrown.expect(XmlRpcException.class);
        thrown.expectMessage("A blog post with title {0} already exists for date:");
        updateBlog(anotherBlog.get().getId().asLong(),
                BLOG_TITLE_1, BLOG_CONTENTS_EDITED,
                null,
                true);
    }

    @Test
    public void testGetBlogPost() throws XmlRpcException, IOException {
        final String blogTitle = "Labels " + BLOG_TITLE_1;
        final String blogId = createNewBlog(
                space.get().getKey(),
                blogTitle,
                BLOG_CONTENTS_1,
                new String[]{"label01", "label02"},
                true).toString();

        final Map<String, Object> blogStructure = (Map) xmlRpcClient.execute("metaWeblog.getPost",
                new Vector<>(
                        asList(
                                new Object[]{
                                        blogId.toString(),
                                        user.get().getUsername(),
                                        user.get().getPassword()
                                }
                        )
                )
        );

        assertNotNull(blogStructure);

        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");

        assertThat(
                blogStructure,
                allOf(
                        hasEntry("author", user.get().getUsername()),
                        hasEntry("title", blogTitle),
                        hasEntry("description", BLOG_CONTENTS_1),
                        hasEntry("postid", blogId),
                        hasEntry("blogid", space.get().getKey())
                )
        );
        String blogUrl;
        if (restClient.getAdminSession().darkFeature().isSiteFeatureEnabled("confluence.readable.url")) {
            blogUrl = format(
                    "%s//spaces/%s/blog/%s/%s/%s",
                    baseUrlSelector.getBaseUrl(),
                    space.get().getKey(),
                    simpleDateFormat.format(blogStructure.get("dateCreated")),
                    blogId,
                    urlEncode(blogTitle.replaceAll("!", ""))
            );
        } else {
            blogUrl = format(
                    "%s//pages/viewpage.action?pageId=%s",
                    baseUrlSelector.getBaseUrl(),
                    blogId
            );
        }

        assertThat(
                blogStructure, allOf(
                        hasEntry("link", blogUrl),
                        hasEntry("permaLink", blogUrl)));

        assertThat(
                simpleDateFormat.format(blogStructure.get("dateCreated")),
                is(simpleDateFormat.format(new Date()))
        );
        assertThat(
                simpleDateFormat.format(blogStructure.get("pubDate")),
                is(simpleDateFormat.format(new Date()))
        );

        final List<String> blogLabels = (List) blogStructure.get("categories");
        assertNotNull(blogLabels);
        assertThat(blogLabels, containsInAnyOrder("label01", "label02"));
    }

    @ResetFixtures(value = {"page", "anotherPage"}, when = ResetFixtures.When.BOTH)
    @Test
    public void testGetCategories() throws XmlRpcException, IOException {
        final Collection<Label> spaceLabels = new ArrayList<>(21);
        for (int i = 1; i < 20; ++i) {
            spaceLabels.add(Label.builder("label" + i).build());
        }

        restClient.createSession(user.get())
                .contentLabelService()
                .addLabels(page.get().getId(), spaceLabels);

        restClient.createSession(user.get())
                .contentLabelService()
                .addLabels(anotherPage.get().getId(), asList(Label.builder("label19").build(), Label.builder("label18").build()));

        Map<?, ?> categories = (Map) xmlRpcClient.execute(
                "metaWeblog.getCategories",
                new Vector<>(
                        asList(
                                new Object[] {
                                        anotherSpace.get().getKey(),
                                        user.get().getUsername(),
                                        user.get().getPassword()
                                }
                        )
                )
        );

        assertNotNull(categories);
        assertThat(categories.size(), is(19));

        // metaWeblog.getCategories is getting by most 20 most popular labels not base on name order.
        assertThat(
                spaceLabels.stream().map(Label::getLabel).collect(toList()),
                containsInAnyOrder(categories.keySet().toArray())
        );


        for (Map.Entry<?, ?> entry : categories.entrySet()) {
            final String labelName = (String) ((Map.Entry) entry).getKey();
            final Map<?, ?> labelStructure = (Map) ((Map.Entry) entry).getValue();

            assertThat(labelStructure.get("description"), is(labelName));
            assertNotNull(labelStructure.get("htmlUrl"));
            assertNotNull(labelStructure.get("rssUrl"));
        }
    }

    @Test
    public void testGetRecentPostsLimitedByMaxPosts() throws XmlRpcException, IOException {
        restClient.createSession(user.get())
                .contentLabelService()
                .addLabels(blog.get().getId(), asList(
                        Label.builder("recent-label-01").build(),
                        Label.builder("recent-label-02").build()
                ));

        /* At this point, we will have two blogs (one of them is the Octagon blog post).
         * We want to test the remote methods ability to return a limited number of posts
         */
        final List<Map<?, ?>> blogs = (List) xmlRpcClient.execute(
                "metaWeblog.getRecentPosts",
                new Vector(
                        Arrays.asList(
                                new Object[] {
                                        space.get().getKey(),
                                        user.get().getUsername(),
                                        user.get().getPassword(),
                                        1
                                }
                        )
                )
        );

        assertNotNull(blogs);
        assertThat(blogs, hasSize(1));

        final Map<?, ?> blogStructure = blogs.get(0);
        final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM/dd");

        assertThat(
                blogStructure,
                allOf(
                        hasEntry("author", user.get().getUsername()),
                        hasEntry("title", blog.get().getTitle()),
                        hasEntry("description", BLOG_CONTENTS_1),
                        hasEntry("postid", blog.get().getId().serialise()),
                        hasEntry("blogid", space.get().getKey())
                )
        );
        String blogUrl;
        if (restClient.getAdminSession().darkFeature().isSiteFeatureEnabled("confluence.readable.url")) {
            blogUrl = format(
                    "%s//spaces/%s/blog/%s/%s/%s",
                    baseUrlSelector.getBaseUrl(),
                    space.get().getKey(),
                    simpleDateFormat.format(blogStructure.get("dateCreated")),
                    blog.get().getId().serialise(),
                    urlEncode(blog.get().getTitle().replaceAll("!", ""))
            );
        } else {
            blogUrl = format(
                    "%s//pages/viewpage.action?pageId=%d",
                    baseUrlSelector.getBaseUrl(),
                    blog.get().getId().asLong()
            );
        }

        assertThat(
                blogStructure, allOf(
                        hasEntry("link", blogUrl),
                        hasEntry("permaLink", blogUrl)));

        assertThat(simpleDateFormat.format(blogStructure.get("pubDate")), is(simpleDateFormat.format(new Date())));
        assertThat(simpleDateFormat.format(blogStructure.get("dateCreated")), is(simpleDateFormat.format(new Date())));


        final List<String> blogLabels = (List) blogStructure.get("categories");
        assertNotNull(blogLabels);
        assertThat(blogLabels, hasSize(2));
        assertThat(blogLabels, hasItems("recent-label-01", "recent-label-02"));
    }

    private Object createNewBlog(final String spaceKey, final String title, final String content,
                                 final String[] labels, final boolean publish) throws XmlRpcException, IOException {
        final Map<String, Object> blogStructure = toBlogStructure(title, content, labels);

        return xmlRpcClient.execute(
                "metaWeblog.newPost",
                toXmlRpcParams(
                        new Object[]{
                                spaceKey,
                                user.get().getUsername(),
                                user.get().getPassword(),
                                blogStructure,
                                publish ? Boolean.TRUE : Boolean.FALSE
                        }
                )
        );
    }

    private Object updateBlog(final long blogPostId, final String title, final String content, final String[] labels,
                              final boolean publish) throws XmlRpcException, IOException {
        final Map<String, Object> blogStructure = toBlogStructure(title, content, labels);
        return xmlRpcClient.execute(
                "metaWeblog.editPost",
                toXmlRpcParams(
                        new Object[]{
                                String.valueOf(blogPostId),
                                user.get().getUsername(),
                                user.get().getPassword(),
                                blogStructure,
                                publish ? Boolean.TRUE : Boolean.FALSE
                        }
                )
        );
    }

    private Map<String, Object> toBlogStructure(String title, String content, String[] labels) {
        final Map<String, Object> blogStructure = new Hashtable<>();

        blogStructure.put("title", title);
        blogStructure.put("description", content);

        if (null != labels) {
            blogStructure.put("categories", new Vector<>(asList(labels)));
        }

        return blogStructure;
    }

    private void assertBlogUrl(Content content) {
        if (restClient.getAdminSession().darkFeature().isSiteFeatureEnabled("confluence.readable.url")) {
            final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd");
            String creationDate = content.getVersion().getWhenAt().format(dateTimeFormatter);
            assertThat(
                    content.getLinks().get(LinkType.WEB_UI).getPath(),
                    containsString(format("/spaces/%s/blog/%s/%d/%s", space.get().getKey(), creationDate, content.getId().asLong(), urlEncode(content.getTitle().replaceAll("!", ""))))
            );
        } else {
            assertThat(
                    content.getLinks().get(LinkType.WEB_UI).getPath(),
                    is(format("/pages/viewpage.action?pageId=%s", content.getId().serialise()))
            );
        }
    }
}
