package it.com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import java.util.Arrays;
import java.util.Vector;

public class TestUtils {

    public static Vector<Object> toXmlRpcParams(final Object[] params) {
        final Vector<Object> vector = new Vector<>();

        if (null != params) {
            vector.addAll(Arrays.asList(params));
        }

        return vector;
    }
}
