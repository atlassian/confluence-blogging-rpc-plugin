package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertEquals;

public class BloggerImplTest {
    private BloggerImpl blogger;

    @Before
    public void setUp() throws Exception {
        blogger = new BloggerImpl(null, null, null, null, null, null, null);
    }

    @Test
    public void testSplitContent() {
        assertEquals(new HashMap<String, String>() {
            {
                put(BloggerImpl.CONTENT, "contents");
                put(BloggerImpl.TITLE, "title");
            }
        }, blogger.splitBlogContent("<title>title</title>contents"));
    }

    @Test
    public void testSplitTitleTrimmedButNotContent() {
        assertEquals(new HashMap<String, String>() {
            {
                put(BloggerImpl.CONTENT, "\n contents");
                put(BloggerImpl.TITLE, "title");
            }
        }, blogger.splitBlogContent("  \n  <title>title</title>\n contents"));
    }

    @Test
    public void testSplitWithMultipleTitleTags() {
        assertEquals(new HashMap<String, String>() {
            {
                put(BloggerImpl.CONTENT, " some content <title>title 2</title> more content");
                put(BloggerImpl.TITLE, "title one");
            }
        }, blogger.splitBlogContent("<title>title one</title> some content <title>title 2</title> more content"));
    }

    @Test
    public void testSplitWithUnclosedTitleTag() {
        assertEquals(new HashMap<String, String>() {
            {
                put(BloggerImpl.CONTENT, "<title>invalid title<br> contents");
            }
        }, blogger.splitBlogContent("<title>invalid title<br> contents"));
    }

    @Test
    public void testSplitWithMultipleUnclosedTitleTags() {
        assertEquals(new HashMap<String, String>() {
            {
                put(BloggerImpl.CONTENT, "<title>invalid title<title> contents");
            }
        }, blogger.splitBlogContent("<title>invalid title<title> contents"));
    }

    @Test
    public void testSplitWithoutContent() {
        assertEquals(new HashMap<String, String>() {
            {
                put(BloggerImpl.TITLE, "title");
                put(BloggerImpl.CONTENT, "");
            }
        }, blogger.splitBlogContent("<title>title</title>"));
    }

    @Test
    public void testSplitWithBlank() {
        assertEquals(new HashMap<String, String>() {
            {
                put(BloggerImpl.CONTENT, "");
            }
        }, blogger.splitBlogContent(""));
    }
}