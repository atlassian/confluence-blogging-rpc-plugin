package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.api.service.accessmode.AccessModeService;
import com.atlassian.confluence.impl.labels.LabelPermissionEnforcerImpl;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelPermissionEnforcer;
import com.atlassian.confluence.labels.persistence.dao.LabelSearchResult;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Arrays;
import java.util.Hashtable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestMetaWeblogGetCategories {
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private AccessModeService accessModeService;

    @Mock
    private LabelManager labelManager;

    @Mock
    private ApplicationProperties applicationProperties;

    private LabelPermissionEnforcer labelPermissionEnforcer;

    private MetaWeblogImpl metaWeblog;

    @Before
    public void setUp() {
        labelPermissionEnforcer = new LabelPermissionEnforcerImpl(permissionManager, accessModeService);

        metaWeblog = new MetaWeblogImpl(bloggingUtils, new TransactionTemplate() {
            @Override
            public <T> T execute(TransactionCallback<T> tTransactionCallback) {
                return tTransactionCallback.doInTransaction();
            }
        }, spaceManager, spacePermissionManager, pageManager, permissionManager, labelManager, labelPermissionEnforcer, applicationProperties);
        when(bloggingUtils.getText(anyString(), Matchers.<String[]>anyObject())).thenAnswer(
                (Answer<String>) invocationOnMock -> invocationOnMock.getArguments()[0].toString()
        );

        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://localhost:1990/confluence");
    }

    @After
    public void tearDown() {
        bloggingUtils = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        labelManager = null;
        applicationProperties = null;
    }

    @Test
    public void testGetCategoriesFromUnknownSpace() {
        try {
            metaWeblog.getCategories("", "", "");
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.space.unknown", re.getMessage());
        }
    }

    @Test
    public void testGetCategoriesWithoutSufficientPermission() {
        Space space = new Space("TST");
        when(spaceManager.getSpace(space.getKey())).thenReturn(space);

        try {
            metaWeblog.getCategories(space.getKey(), "", "");
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.permission.view.space.blog", re.getMessage());
        }
    }

    @Test
    public void testGetCategoriesLabellessSpace() throws RemoteException {
        Space space = new Space("TST");
        when(spaceManager.getSpace(space.getKey())).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.VIEWSPACE_PERMISSION, space, null)).thenReturn(true);
        assertEquals(new Hashtable<String, String>(), metaWeblog.getCategories(space.getKey(), "", ""));
    }

    @Test
    public void testGetCategoriesReturnsBothPopularAndSpaceLabels() throws RemoteException {
        Space space = new Space("TST");
        when(spaceManager.getSpace(space.getKey())).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.VIEWSPACE_PERMISSION, space, null)).thenReturn(true);
        when(labelManager.getLabelsInSpace(space.getKey())).thenReturn(
                Arrays.asList(new Label("space-label"))
        );

        LabelSearchResult popularLabelResult = mock(LabelSearchResult.class);
        when(popularLabelResult.getLabel()).thenReturn(new Label("popular-label"), new Label("space-label"));
        when(labelManager.getMostPopularLabels(anyInt())).thenReturn(Arrays.asList(popularLabelResult));

        Hashtable result = metaWeblog.getCategories(space.getKey(), "", "");
        assertNotNull(result);
        assertEquals(2, result.size());
        assertTrue(result.containsKey("popular-label"));
        assertTrue(result.containsKey("space-label"));
    }
}

