package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelPermissionEnforcer;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.features.DarkFeatureManager;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.spring.container.ContainerContext;
import com.atlassian.spring.container.ContainerManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Date;
import java.util.Hashtable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.Silent.class)
public class TestMetaWeblogGetPost {
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private LabelManager labelManager;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private LabelPermissionEnforcer labelPermissionEnforcer;

    private MetaWeblogImpl metaWeblog;

    @Before
    public void setUp() {
        when(bloggingUtils.getText(anyString(), Matchers.<String[]>anyObject())).thenAnswer(
                (Answer<String>) invocationOnMock -> invocationOnMock.getArguments()[0].toString()
        );

        when(bloggingUtils.getText(anyString())).thenAnswer(
                (Answer<String>) invocationOnMock -> invocationOnMock.getArguments()[0].toString()
        );

        when(applicationProperties.getBaseUrl(UrlMode.CANONICAL)).thenReturn("http://localhost:1990/confluence");
        ContainerContext containerContext = mock(ContainerContext.class);
        ContainerManager.getInstance().setContainerContext(containerContext);
        when(containerContext.getComponent("salDarkFeatureManager")).thenReturn(mock(DarkFeatureManager.class));

        metaWeblog = new MetaWeblogImpl(bloggingUtils, new TransactionTemplate() {
            @Override
            public <T> T execute(TransactionCallback<T> tTransactionCallback) {
                return tTransactionCallback.doInTransaction();
            }
        }, spaceManager, spacePermissionManager, pageManager, permissionManager, labelManager, labelPermissionEnforcer, applicationProperties);
    }

    @After
    public void tearDown() {
        bloggingUtils = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        labelManager = null;
        applicationProperties = null;
    }

    @Test
    public void testGetPostWithNonNumericId() {
        try {
            metaWeblog.getPost("foobar", "", "");
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.blog.invalidpostid", re.getMessage());
        }
    }

    @Test
    public void testGetPostWithInvalidId() {
        try {
            metaWeblog.getPost(String.valueOf(0), "", "");
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.blog.doesnotexists", re.getMessage());
        }
    }

    @Test
    public void testGetRestrictedPost() {
        when(pageManager.getBlogPost(0L)).thenReturn(new BlogPost());

        try {
            metaWeblog.getPost(String.valueOf(0), "", "");
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.permission.view.blog", re.getMessage());
        }
    }

    @Test
    public void testGetPost() throws RemoteException {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);

        BlogPost bp = new BlogPost();
        bp.setSpace(space);
        bp.setTitle("foobar");
        bp.setBodyAsString("barfoo");

        long postId = 1;
        bp.setId(postId);

        Date now = new Date();
        bp.setCreationDate(now);

        when(pageManager.getBlogPost(postId)).thenReturn(bp);
        when(permissionManager.hasPermission(Matchers.anyObject(), eq(Permission.VIEW), eq(bp))).thenReturn(true);
        when(bloggingUtils.convertStorageFormatToView(bp)).thenReturn(bp.getBodyAsString());


        Hashtable post = metaWeblog.getPost(String.valueOf(postId), "", "");
        assertNotNull(post);

        assertEquals(bp.getTitle(), post.get(MetaWeblogImpl.TITLE));
        assertEquals(bp.getBodyAsString(), post.get(MetaWeblogImpl.DESCRIPTION));
        assertEquals(bp.getIdAsString(), post.get(MetaWeblogImpl.POSTID));
    }
}