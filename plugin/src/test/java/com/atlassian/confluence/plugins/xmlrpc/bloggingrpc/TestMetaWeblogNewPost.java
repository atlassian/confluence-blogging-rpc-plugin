package com.atlassian.confluence.plugins.xmlrpc.bloggingrpc;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.labels.LabelPermissionEnforcer;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.rpc.RemoteException;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.security.SpacePermission;
import com.atlassian.confluence.security.SpacePermissionManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.Calendar;
import java.util.Hashtable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TestMetaWeblogNewPost {
    @Mock
    private BloggingUtils bloggingUtils;

    @Mock
    private SpaceManager spaceManager;

    @Mock
    private SpacePermissionManager spacePermissionManager;

    @Mock
    private PageManager pageManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private LabelManager labelManager;

    @Mock
    private ApplicationProperties applicationProperties;

    @Mock
    private LabelPermissionEnforcer labelPermissionEnforcer;

    @Mock
    private ConfluenceUser user;

    private MetaWeblogImpl metaWeblog;

    @Before
    public void setUp() {
        metaWeblog = new MetaWeblogImpl(bloggingUtils, new TransactionTemplate() {
            @Override
            public <T> T execute(TransactionCallback<T> tTransactionCallback) {
                return tTransactionCallback.doInTransaction();
            }
        }, spaceManager, spacePermissionManager, pageManager, permissionManager, labelManager, labelPermissionEnforcer, applicationProperties);

        when(bloggingUtils.getText(anyString(), Matchers.<String[]>anyObject())).thenAnswer(
                (Answer<String>) invocationOnMock -> invocationOnMock.getArguments()[0].toString()
        );

        when(bloggingUtils.getText(anyString())).thenAnswer(
                (Answer<String>) invocationOnMock -> invocationOnMock.getArguments()[0].toString()
        );
    }

    @After
    public void tearDown() {
        bloggingUtils = null;
        spaceManager = null;
        spacePermissionManager = null;
        pageManager = null;
        permissionManager = null;
        labelManager = null;
        applicationProperties = null;
        user = null;
    }

    @Test
    public void testPostInNonExistentSpace() {
        try {
            metaWeblog.newPost("TST", "", "", null, true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.space.unknown", re.getMessage());
        }
    }

    @Test
    public void testPostWithoutSufficientPrivileges() {
        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        try {
            metaWeblog.newPost(spaceKey, "", "", null, true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.permission.edit.space.blog", re.getMessage());
        }
    }

    @Test
    public void testPostWithoutTitle() throws RemoteException {
        String userName = "john.doe";
        String password = "";
        when(bloggingUtils.authenticateUser(userName, password)).thenReturn(user);

        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user)).thenReturn(true);

        try {
            metaWeblog.newPost(spaceKey, userName, password, new Hashtable<>(), true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.blog.title.blank", re.getMessage());
        }
    }

    @Test
    public void testCreateDuplicateBlogPost() throws RemoteException {
        String userName = "john.doe";
        String password = "";
        when(bloggingUtils.authenticateUser(userName, password)).thenReturn(user);

        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user)).thenReturn(true);
        when(pageManager.getBlogPost(eq(space.getKey()), eq("Title"), Matchers.<Calendar>anyObject())).thenReturn(new BlogPost());

        try {
            metaWeblog.newPost(spaceKey, userName, password, new Hashtable<String, Object>() {
                {
                    put(MetaWeblogImpl.TITLE, "Title");
                }
            }, true);
            fail("RemoteException should've been raised");
        } catch (RemoteException re) {
            assertEquals("error.validation.blog.duplicate", re.getMessage());
        }
    }

    @Test
    public void testPostWithSufficientPrivileges() throws RemoteException {
        String userName = "john.doe";
        String password = "";
        when(bloggingUtils.authenticateUser(userName, password)).thenReturn(user);

        String spaceKey = "TST";
        Space space = new Space(spaceKey);
        when(spaceManager.getSpace(spaceKey)).thenReturn(space);

        when(spacePermissionManager.hasPermission(SpacePermission.EDITBLOG_PERMISSION, space, user)).thenReturn(true);

        metaWeblog.newPost(spaceKey, userName, password, new Hashtable<String, Object>() {
            {
                put(MetaWeblogImpl.TITLE, "Title");
                put(MetaWeblogImpl.DESCRIPTION, "Content");
            }
        }, true);
        verify(pageManager).saveContentEntity(argThat(
                post -> StringUtils.equals("Title", post.getTitle())
                        && StringUtils.equals("Content", post.getBodyAsString())
        ), isNull());
    }
}